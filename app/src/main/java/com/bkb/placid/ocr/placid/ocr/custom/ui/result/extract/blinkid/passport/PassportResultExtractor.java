package com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.passport;

import com.microblink.entities.recognizers.blinkid.passport.PassportRecognizer;
import com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.BlinkIdExtractor;

public class PassportResultExtractor extends BlinkIdExtractor<PassportRecognizer.Result, PassportRecognizer> {

    @Override
    protected void extractData(PassportRecognizer.Result result) {
        extractMRZResult(result.getMrzResult());
    }

}
