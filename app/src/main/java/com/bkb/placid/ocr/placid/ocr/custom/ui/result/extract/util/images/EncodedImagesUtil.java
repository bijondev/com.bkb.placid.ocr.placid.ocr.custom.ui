package com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.util.images;

public class EncodedImagesUtil {

    public static boolean shouldShowEncodedImageEntry(byte[] encodedImage) {
        return encodedImage != null && encodedImage.length > 0;
    }

}
