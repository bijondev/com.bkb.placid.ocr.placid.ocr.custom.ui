package com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.documentface;

import com.microblink.entities.recognizers.blinkid.documentface.DocumentFaceRecognizer;
import com.bkb.placid.ocr.placid.ocr.custom.ui.R;
import com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.BlinkIdExtractor;

public class DocumentFaceRecognitionResultExtractor extends BlinkIdExtractor<DocumentFaceRecognizer.Result, DocumentFaceRecognizer> {

    @Override
    protected void extractData(DocumentFaceRecognizer.Result result) {
        add(R.string.MBDocumentLocation, result.getDocumentLocation().toString());
        add(R.string.MBFaceLocation, result.getFaceLocation().toString());
    }
}