package com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.visa;

import com.microblink.entities.recognizers.blinkid.visa.VisaRecognizer;
import com.bkb.placid.ocr.placid.ocr.custom.ui.result.extract.blinkid.BlinkIdExtractor;

public class VisaRecognizerResultExtractor extends BlinkIdExtractor<VisaRecognizer.Result, VisaRecognizer> {

    @Override
    protected void extractData(VisaRecognizer.Result result) {
        extractMRZResult(result.getMrzResult());
    }

}
